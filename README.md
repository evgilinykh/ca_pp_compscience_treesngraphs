# ca_pp_treesngraphs



## What is it?

This is our portofolio project for Computer Science course.

Section: Trees and Graphs

## Description

In this project we created terminal game where you are manager of CatFeed Co. in New York, NY.

- Your goal is patrol city at night and feed homeless cats until Big Sponsor will appear and give you a lot of money to build shleter for poor cats 🐈🐈🐈🐈🐈

- You could patrol by different types of transport - on foot, bysicle, motorbike, car, wan. Every type of vehicle has each own speed, food capacity and gas consumption.

- You could buy a navigator - it would help you to build the best route.

- Also you could rent a warehouse to store catFood.

- And you need to buy catFood.

- While there is no big sponsor you will be given money by simple citizens.

- For every well-fed cat you will get reputation points. They will affect number and amount of donations.

- For every cat you missed or don't find - you will loose reputation points.

- Because of NY NY is a big city with a lot of noise - all cats hide after night is end, so you need to do your job in period from 23:00 to 6:00.

- This game will help you to improve you manage and financial skills.




## Features

- Save/load game
- Use map of city
- Rent warehouse
- Buy navigator
- Buy vehicles
- Hungry kittens!!!!!
